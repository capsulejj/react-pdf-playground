import React, { useState, useEffect } from "react";
import {
  Page,
  Text,
  Image,
  View,
  Document,
  StyleSheet,
  Font,
} from "@react-pdf/renderer";
import { PDFViewer } from "@react-pdf/renderer";
import sf_logo from "./asset/SF_Express_Logo.png";
import sf_hk_phone from "./asset/SF_Express_Contact.png";
import to_zh from "./asset/to_zh.png";
import from_zh from "./asset/from_zh.png";
import A from "./asset/A.jpg";
import sampleQR from "./asset/sampleQR.jpeg";
import STSong from "./asset/stsong.ttf";
import Arial from "./asset/arial.ttf";
import JsBarcode from "jsbarcode";
import QRCode from "qrcode";
// Create styles

// TODO:
/**
 * Unknown Issue
 * 1. Font-size (unmatched size, "pt" unit is required in API docs, but we use "px" unit instead)
 * 2. Font-family (specified in API docs is unmatched with "HK BSP Waybill Template")
 * 3. Image
 *    3.1 image size
 *      - shipper's "from" icon is larger than container,
 *      - images size in Icon Area is not sure)
 *    3.2 SF Logo style
 *      - unknown border style for SF Logo
 * 4. Lack of PDF data elements (Data not provided in response object)
 * 5. Remarks Area (No example for references)
 */

Font.register({
  family: "STSong",
  src: STSong,
});

Font.register({
  family: "Arial",
  src: Arial,
});
Font.register({
  family: "msyh",
  src: "https://do6lqjwiviruo.cloudfront.net/build/msyh.ttf",
});

const styles = StyleSheet.create({
  page: {
    position: "relative",
    fontFamily: "msyh",
    flexDirection: "column",
    padding: "5px",
  },
  container: {
    height: "100%",
    flexDirection: "column",
    border: "0.25px",
    borderColor: "#000",
  },
  row0: {
    height: "9.3%", // 14mm row height, // * 14/150*100% = 9.3%, where 14 is row height(in mm), 150 is total height(in mm)
    flexDirection: "column",
    borderBottom: "0.25px",
    borderColor: "#000",
  },
  row0_logo: {
    flexDirection: "row",
    flexShrink: 1,
  },
  row0_soruceAndTime: {
    flexShrink: 0,
    fontSize: "5px",
    paddingTop: "3%", // 空白區域高 5mm
    paddingLeft: "5%",
  },
  row1: {
    height: "16%", // 24mm row height
    flexDirection: "column",
    alignItems: "center",
    borderBottom: "0.25px",
    borderColor: "#000",
  },
  row1_barcode: {
    height: "54%", // 13mm barcode height
    width: "80%", // 80mm barcode width
  },
  row1_trackingNumber: {
    flexDirection: "column",
    fontSize: "10px",
  },
  row2: {
    height: "6.7%", // 10mm row height
    fontSize: "20px",
    fontWeight: "bold",
    alignItems: "center",
    borderBottom: "0.25px",
    borderColor: "#000",
  },
  row3: {
    height: "6.7%", // 10mm row height
    flexDirection: "row",
    borderBottom: "0.25px",
    borderColor: "#000",
    alignItems: "center",
    // padding: "0 2% 0 2%",
  },
  row3_toLogo: {
    fontSize: "12px",
    // height: "70%", // 7mm logo size
  },
  row3_recipientInfo: {
    fontSize: "7px",
    flexDirection: "column",
    paddingLeft: "10px",
  },
  row4: {
    height: "21.3%", // combine 6mm + 8mm + 18mm = 32mm row
    flexDirection: "row",
    borderBottom: "0.25px",
    borderColor: "#000",
  },
  row4_col1: {
    width: "50%", // 50mm col width
    flexDirection: "column",
    borderRight: "0.25px",
    borderColor: "#000",
  },
  row4_col2: {
    width: "32%", // 32mm col width
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    borderRight: "0.25px",
    borderColor: "#000",
  },
  row4_col3: {
    width: "18%", // 18mm col width
    flexDirection: "column",
    alignItems: "center",
  },
  row4_senderRow: {
    height: "18.75%", // 32mm * 18.75% = 6mm row height
    flexDirection: "row",
    borderBottom: "0.25px",
    borderColor: "#000",
    alignItems: "center",
  },
  row4_fromLogo: {
    fontSize: "12px",
    // height: "117%", // 6mm * 117% = 7mm logo size
  },
  row4_senderInfo: {
    flexDirection: "column",
    paddingLeft: "10px",
    fontSize: "4px",
  },
  row4_signRow: {
    height: "25%", // 32mm * 25% = 8mm row height
    flexDirection: "column",
    fontSize: "8px",
    borderBottom: "0.25px",
    borderColor: "#000",
  },
  row4_codingMappingRow: {
    height: "56.25%", // 32mm * 56.25% = 18mm row height
    fontSize: "25px",
  },
  row4_qrCode: {
    height: "78.125%", // 32mm * 78.125% = 25mm qrCode size
    width: "78.125%",
  },
  row4_tagLogo: {
    height: "45%", // 唔識計
    maxWidth: "80%",
  },
  row5: {
    height: "12%", // 18mm row height
    flexDirection: "row",
    justifyContent: "space-between",
    borderBottom: "0.25px",
    borderColor: "#000",
  },
  row5_logo_row: {
    flexDirection: "column",
    flexShrink: 1,
  },
  row5_barcode_row: {
    width: "66%", // 66mm row width
    flexDirection: "column",
  },
  row5_barcode: {
    height: "72%", // 18mm * 72% = 13mm barcode height
  },
  row5_trackingNumber: {
    fontSize: "8px",
    textAlign: "center",
  },
  row6: {
    height: "6.7%", // 10mm height row
    flexDirection: "row",
    borderBottom: "0.25px",
    borderColor: "#000",
  },
  row6_col1: {
    width: "50%", // 50mm column width
    flexDirection: "row",
    alignItems: "center",
    borderRight: "0.25px",
    borderColor: "#000",
  },
  row6_col2: {
    width: "50%", // 50mm column width
    flexDirection: "row",
    alignItems: "center",
  },
  row6_logo: {
    fontSize: "12px",
    // height: "70%", // 7mm logo size
  },
  row6_infoRow: {
    flexDirection: "column",
    paddingLeft: "10px",
    fontSize: "4px",
  },
  row7: {
    height: "4%", // 6mm row height
    fontSize: "10px",
    justifyContent: "center",
  },
  row8: {
    height: "4%", // 26mm row height
    fontSize: "10px",
    justifyContent: "center",
  },
  row9: {
    height: "13.3%",
    fontSize: "5px",
    flexDirection: "row",
  },
  row9_col: {
    flexDirection: "column",
    width: "50%",
  },
  row9_text: {
    flexGrow: 1,
  },
  eng: {
    fontFamily: "Arial",
  },
  proCode: {
    top: "1%",
    right: "3%",
    position: "absolute",
    flexDirection: "row",
    fontSize: "25px",
  },
});
function textToBase64Barcode(text) {
  var canvas = document.createElement("canvas");
  JsBarcode(canvas, text, { format: "CODE128", displayValue: false });
  console.log("AAAA", canvas.toDataURL("image/png"));
  return canvas.toDataURL("image/png");
}

function App({ responseObject }) {
  const { requestInfo, orderResponse, rlsInfo, rlsDetail } = responseObject;
  const { orderId, mailNo, filterResult } = orderResponse;
  const {
    waybillNo,
    cargoTypeCode,
    limitTypeCode,
    expressTypeCode,
    proCode,
    destRouteLabel,
    twoDimensionCode,
    codingMapping,
  } = rlsDetail;
  const { order } = requestInfo;
  const {
    shipperCompany,
    shipperContact,
    shipperTel,
    shipperAddress,
    consigneeCompany,
    consigneeContact,
    consigneeTel,
    consigneeAddress,
    consigneeCountry,
  } = order;
  const waybillNoBase64 = textToBase64Barcode(waybillNo);
  const waybillNoWithDelimiter = waybillNo.match(/.{1,3}/g).join(" ");
  const [qrCode, setQrCode] = useState("");

  useEffect(() => {
    QRCode.toDataURL(twoDimensionCode, { margin: 0 })
      .then((url) => {
        console.log("ON9", url);
        setQrCode(url);
      })
      .catch((err) => {
        console.error("ON9 son", err);
      });
  }, []);

  return (
    <>
      <PDFViewer style={{ width: "100%", height: "100vh" }}>
        <Document>
          <Page size={["286", "429"]} style={styles.page}>
            <View style={styles.container}>
              <View style={styles.row0}>
                <View style={styles.row0_logo}>
                  <Image src={sf_logo} style={{ height: "100%" }} />
                  <Image src={sf_hk_phone} style={{ height: "100%" }} />
                </View>
                <View style={styles.row0_soruceAndTime}>
                  <Text>
                    {/* 第一次打印&nbsp;&nbsp;打印時間&nbsp;&nbsp;2020-05-20&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;12:00&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;第8/10個 */}
                    2020-05-20&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;12:00
                  </Text>
                </View>
              </View>
              <View style={styles.row1}>
                <Image src={waybillNoBase64} style={styles.row1_barcode} />
                <View style={styles.row1_trackingNumber}>
                  <Text>母單號 {waybillNoWithDelimiter}</Text>
                </View>
              </View>
              <View style={styles.row2}>
                <Text>{destRouteLabel}</Text>
              </View>
              <View style={styles.row3}>
                <Text style={styles.row3_toLogo}>收</Text>
                {/* <Image src={to_zh} style={styles.row3_toLogo} /> */}
                <View style={styles.row3_recipientInfo}>
                  <Text>
                    {consigneeContact}&nbsp;&nbsp;{consigneeTel}
                  </Text>
                  <Text>
                    {consigneeAddress}&nbsp;{consigneeCountry}
                  </Text>
                </View>
              </View>
              <View style={styles.row4}>
                <View style={styles.row4_col1}>
                  <View style={styles.row4_senderRow}>
                    <Text style={styles.row4_fromLogo}>寄</Text>
                    {/* <Image style={styles.row4_fromLogo} src={from_zh} /> */}
                    <View style={styles.row4_senderInfo}>
                      <Text>
                        {shipperContact}&nbsp;&nbsp;{shipperTel}
                      </Text>
                      <Text>{shipperAddress}</Text>
                    </View>
                  </View>
                  <View style={styles.row4_signRow}>
                    <Text>已驗視</Text>
                    <Text>簽收：</Text>
                  </View>
                  <View style={styles.row4_codingMappingRow}>
                    <Text>{codingMapping}</Text>
                  </View>
                </View>
                <View style={styles.row4_col2}>
                  {qrCode && <Image src={qrCode} style={styles.row4_qrCode} />}
                </View>
                <View style={styles.row4_col3}>
                  <Image src={A} style={styles.row4_tagLogo} />
                  <Image src={A} style={styles.row4_tagLogo} />
                </View>
              </View>
              <View style={styles.row5}>
                <View style={styles.row5_logo_row}>
                  <Image src={sf_logo} />
                  <Image src={sf_hk_phone} />
                </View>
                <View style={styles.row5_barcode_row}>
                  <Image src={waybillNoBase64} style={styles.row5_barcode} />
                  <Text style={styles.row5_trackingNumber}>
                    {waybillNoWithDelimiter}
                  </Text>
                </View>
              </View>
              <View style={styles.row6}>
                <View style={styles.row6_col1}>
                  <Text style={styles.row6_logo}>寄</Text>
                  {/* <Image src={from_zh} style={styles.row6_logo} /> */}
                  <View style={styles.row6_infoRow}>
                    <Text>
                      {shipperContact}&nbsp;&nbsp;{shipperTel}
                    </Text>
                    <Text>{shipperAddress}</Text>
                  </View>
                </View>
                <View style={styles.row6_col2}>
                  <Text style={styles.row6_logo}>收</Text>
                  {/* <Image src={to_zh} style={styles.row6_logo} /> */}
                  <View style={styles.row6_infoRow}>
                    <Text>
                      {consigneeContact}&nbsp;&nbsp;{consigneeTel}
                    </Text>
                    <Text>
                      {consigneeAddress}&nbsp;{consigneeCountry}
                    </Text>
                  </View>
                </View>
              </View>
              <View style={styles.row7}>
                <Text>訂單號：</Text>
              </View>
              <View style={styles.row8}>
                <Text>托寄物：</Text>
              </View>
              <View style={styles.row9}>
                <View style={styles.row9_col}>
                  <Text style={styles.row9_text}>付款方式：</Text>
                  <Text style={styles.row9_text}>實際重量：</Text>
                  <Text style={styles.row9_text}>申報價值：</Text>
                  <Text style={styles.row9_text}>長*寬*高：</Text>
                </View>
                <View style={styles.row9_col}>
                  <Text style={styles.row9_text}>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;運費：
                  </Text>
                  <Text style={styles.row9_text}>計費重量：</Text>
                  <Text style={styles.row9_text}>其他費用：</Text>
                  <Text style={styles.row9_text}>費用合計：</Text>
                </View>
              </View>
              {/* <View style={styles.section1}>{barcode} </View> */}
              <View style={styles.proCode}>
                {proCode === "T4" ? <Text>特快</Text> : <Text>標快</Text>}
              </View>
            </View>
          </Page>
        </Document>
      </PDFViewer>
    </>
  );
}

export default App;
