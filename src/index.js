import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import "./index.css";
ReactDOM.render(
  <App
    responseObject={{
      requestInfo: {
        accessNumber: "HK_TEST",
        checkWord: "R9CMT1QDMB5BM9VLQP8Z",
        order: {
          orderId: "samuel_order_003",
          shipperCompany: "Receiver Company",
          shipperContact: "Dick",
          shipperTel: "66662222",
          shipperAddress: "Room 2909, GGC, Lai Chi Kok",
          consigneeCompany: "Sender Company",
          consigneeContact: "Donald",
          consigneeTel: "88882222",
          consigneeAddress: "Room 101, KLB",
          consigneeCountry: "Hong Kong",
        },
        cargo: {
          name: "good name",
        },
      },
      orderResponse: {
        orderId: "samuel_order_003",
        mailNo: "SF1030355115133",
        returnTrackingNo: "SF1200020379588",
        filterResult: "2",
        url: "http://ucmp-wx.sit.sf-express.com/wxaccess/weixin/activity/cx_open_order?p1=SF1030355115133",
      },
      rlsInfo: {
        invokeResult: "OK",
        rlsCode: "1000",
      },
      rlsDetail: {
        waybillNo: "SF1030355115133",
        cargoTypeCode: "C201",
        limitTypeCode: "T4",
        expressTypeCode: "B1",
        proCode: "T4",
        destRouteLabel: "775W-BA-010-123456",
        twoDimensionCode:"sample twoDimensionCode sample twoDimensionCode sample twoDimensionCode sample twoDimensionCode",
        codingMapping:"S10"
      },
    }}
  />,
  document.getElementById("root")
);
